<?php

namespace App\Presenters;

use Nette,
    Nette\Application\UI\Form;

use App\Model\Item,
    App\Model\MagicCalculator;


class HomepagePresenter extends Nette\Application\UI\Presenter
{
    /** @var  Item @inject */
    public $item;
    /** @var  MagicCalculator @inject */
    public $calc;

    protected function createComponentWriteOffForm()
    {
        $class = [
            '1' => '1 (3 roky)',
            '2' => '2 (5 let)',
            '3' => '3 (10 let)',
            '4' => '4 (20 let)',
            '5' => '5 (30 let)',
            '6' => '6 (50 let)',
        ];

        $type = [
            '1' => 'Lineární',
            '2' => 'Zrychlený',
        ];

        $form = new Form;
        $form->addText('price')
            ->setAttribute('class', 'form-control')
            ->setRequired();

        $form->addText('year')
            ->setType('number')
            ->setAttribute('class', 'form-control')
            ->setValue(date('Y'))
            ->setRequired();

        $form->addSelect('class', '', $class)
            ->setAttribute('class', 'form-control');

        $form->addSelect('type', '', $type)
            ->setAttribute('class', 'form-control');

        $form->addSubmit('send', 'Vypočítej')
            ->setAttribute('class', 'form-control');

        $form->onSuccess[] = array($this, 'writeOffFormSucceeded');

        return $form;
    }

    public function writeOffFormSucceeded($form, $values)
    {
        if($values->price >= 40000){
            $this->item->set($values);
        }else{
            $this->flashMessage("Cena musí být alespoň 40 000Kč");
        }
    }

    public function renderDefault()
    {
        $this->template->yearPurchase = $this->item->getYearPurchase();
        $this->template->writeOff = $this->calc->magic($this->item);
    }
}

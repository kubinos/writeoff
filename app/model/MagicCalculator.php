<?php

namespace App\Model;


class MagicCalculator
{

    private $classLinear = [
      '1' => [1 => 3, 2 => 20, 3 => 40],
      '2' => [1 => 5, 2 => 11, 3 => 22.25],
      '3' => [1 => 10, 2 => 5.5, 3 => 10.5],
      '4' => [1 => 20, 2 => 2.15, 3 => 5.15],
      '5' => [1 => 30, 2 => 1.4, 3 => 3.4],
      '6' => [1 => 50, 2 => 1.02, 3 => 2.02],
    ];

    private $classFast = [
        '1' => [1 => 3, 2 => 3, 3 => 5],
        '2' => [1 => 5, 2 => 5, 3 => 6],
        '3' => [1 => 10, 2 => 10, 3 => 11],
        '4' => [1 => 20, 2 => 20, 3 => 21],
        '5' => [1 => 30, 2 => 30, 3 => 31],
        '6' => [1 => 50, 2 => 50, 3 => 51],
    ];

    public function magic(Item $item){

        switch ($item->getType()){
            case 1:
                return $this->linear($item);
                break;
            case 2:
                return $this->fast($item);
                break;
        }
    }

    private function linear(Item $item)
    {
        $return = [];
        for($i = 0; $i < $this->classLinear[$item->getClass()][1]; $i++){
            if($i == 0){
                $k = $this->classLinear[$item->getClass()][2];
            }else{
                $k = $this->classLinear[$item->getClass()][3];
            }
            $return[$i] = ceil(($item->getPrice() * $k) / 100);
        }
        return $return;
    }

    private function fast(Item $item)
    {
        $return = [];
        $balance = null;
        for($i = 0; $i < $this->classFast[$item->getClass()][1]; $i++){
            if($i == 0){
                $k = $this->classFast[$item->getClass()][2];
                $off = ceil($item->getPrice() / $k);
                $balance = $item->getPrice() - $off;
                $return[$i] = $off;
            }else{
                $k = $this->classFast[$item->getClass()][3];
                $off  = ceil(($balance * 2) / ($k - $i));
                $balance = $balance - $off;
                $return[$i] = $off;
            }
        }
        return $return;
    }
}
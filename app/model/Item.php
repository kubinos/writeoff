<?php

namespace App\Model;


class Item
{
    private $price;
    private $name;
    private $class;
    private $type;
    private $yearPurchase;

    /**
     * @param mixed $value
     */
    public function set($value)
    {
        $this->price = $value->price;
        $this->class = $value->class;
        $this->yearPurchase = $value->year;
        $this->type = $value->type;
    }

    /**
     * @return mixed
     */
    public function getPrice()
    {
        return $this->price;
    }

    /**
     * @param mixed $price
     */
    public function setPrice($price)
    {
        $this->price = $price;
    }

    /**
     * @return mixed
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param mixed $name
     */
    public function setName($name)
    {
        $this->name = $name;
    }


    /**
     * @return mixed
     */
    public function getYearPurchase()
    {
        return $this->yearPurchase;
    }

    /**
     * @param mixed $yearPurchase
     */
    public function setYearPurchase($yearPurchase)
    {
        $this->yearPurchase = $yearPurchase;
    }

    /**
     * @return mixed
     */
    public function getClass()
    {
        return $this->class;
    }

    /**
     * @param mixed $class
     */
    public function setClass($class)
    {
        $this->class = $class;
    }

    /**
     * @return mixed
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * @param mixed $type
     */
    public function setType($type)
    {
        $this->type = $type;
    }
}